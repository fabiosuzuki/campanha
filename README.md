*** Estratégia utilizada para a criação da aplicação. ***
1 - Escolha das tecnologias a serem utilizadas na aplicação
	Java8 - Por se tratar da última versão estável e por trazer grandes melhorias
	Jetty - Por ser um servidor embarcado que roda a partir de um jar e apartir de um tunnig consegue receber até 2000 requisições por segundos (números alcançados em da aplicação em produção em um dos principais sistemas na bolsa de valores)

2 - Identificação das classes de modelo e elaboração do MER inicial (ver imagem MER.png)

3 - Definição da arquitetura, divisão dos serviços, classes de negocio, dao e modelagem das entidades

4 - Elaboração do diagrama de classes (ver imagem Class Diagram.png)

5 - Implementação da solução

6 - Elaboração de Junit tests para validação e documentação da solução

7 - Criação do projeto campanha-client para simular o cliente enviando requisições e acompanhar a correta aplicação das regras de negócios e persitência no banco de dados.

8 - Atualização da documentação quando necessária
************************************************************************************************************************************************


Resposta Questão 3:

	A implementação esta no pacote /campanha-server/src/main/java/br/com/exercicio3.
	E o testes estão no pacote /campanha-server/src/test/java/br/com/exercicio3


Resposta Questão 4:




Resposta Questão 5:
	Stream realiza o processamento serialmente em single thread, já com ParallelStreams a JVM divide os dados em threads para processamento em paralelo, para processamentos com pouca quantidade de dados se pode usar Stream, agora para grandes quantidades de dados já é mais apropriado a utilização do ParallelStreams.
