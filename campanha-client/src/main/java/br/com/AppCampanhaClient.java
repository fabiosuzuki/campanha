package br.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AppCampanhaClient {

	public static void main(String[] args) {

		try {
			test();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private static void test() throws Exception {
		String input1 = "{\"id\":1,\"dataInicioVigencia\":\"2017-10-01T18:02:52.249Z\",\"dataFimVigencia\":\"2017-10-03T18:02:52.249Z\",\"nomeCampanha\":\"campanha 1\"}";
		send(input1);
		read("{}");
	}

	private static void cenarioDaProva() throws Exception {
		String input1 = "{\"id\":1,\"dataInicioVigencia\":\"2017-10-01T18:02:52.249Z\",\"dataFimVigencia\":\"2017-10-03T18:02:52.249Z\",\"nomeCampanha\":\"campanha 1\"}";
		String input2 = "{\"id\":2,\"dataInicioVigencia\":\"2017-10-01T18:02:52.249Z\",\"dataFimVigencia\":\"2017-10-02T18:02:52.249Z\",\"nomeCampanha\":\"campanha 2\"}";
		String input3 = "{\"id\":3,\"dataInicioVigencia\":\"2017-10-01T18:02:52.249Z\",\"dataFimVigencia\":\"2017-10-03T18:02:52.249Z\",\"nomeCampanha\":\"campanha 3\"}";

		send(input1);
		send(input2);
		send(input3);
		read("{}");
	}

	private static void send(String input) throws IOException {
		URL url = new URL("http://localhost:8080/campanha/create");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		OutputStream os = connection.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		connection.disconnect();
	}
	
	private static void read(String input) throws IOException {
		URL url = new URL("http://localhost:8080/campanha/read");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		OutputStream os = connection.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		connection.disconnect();
	}

}
