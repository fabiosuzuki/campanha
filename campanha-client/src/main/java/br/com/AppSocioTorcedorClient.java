package br.com;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AppSocioTorcedorClient {

	public static void main(String[] args) {

		try {
			insert();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void insert() throws Exception {
		URL url = new URL("http://localhost:8080/socioTorcedor/cadastrar");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		String input = "{\"id\":1,\"nomeCompleto\":\"Fabio Suzuki\",\"email\":\"fabio@suzuki.com\",\"dataNascimento\":\"1980-05-04T18:02:52.249Z\",\"status\":\"Ativo\",\"time\":\"Palmeiras\"}";
//		String input = "{\"id\":1,\"nomeCompleto\":\"Jessica Suzuki\",\"email\":\"jessica@suzuki.com\",\"dataNascimento\":\"1983-12-26T18:02:52.249Z\",\"status\":\"Ativo\",\"time\":\"São Paulo\"}";
//		String input = "{\"id\":1,\"nomeCompleto\":\"Rodolfo Suzuki\",\"email\":\"rodolfo@suzuki.com\",\"dataNascimento\":\"1990-11-21T18:02:52.249Z\",\"status\":\"Ativo\",\"time\":\"Santos\"}";
		
		OutputStream os = connection.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		connection.disconnect();
	}

}
