package br.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TimeCoracaoClient {

	public static void main(String[] args) {

		try {
			cenarioDaProva();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void cenarioDaProva() throws Exception {
		String input1 = "{\"id\":1,\"dataFundacao\":\"1912-04-14T18:02:52.249Z\",\"nome\":\"Santos Futebol Clube\"}";
		String input2 = "{\"id\":2,\"dataFundacao\":\"1922-02-12T18:02:52.249Z\",\"nome\":\"Sociedade Esportiva Palmeiras\"}";
		String input3 = "{\"id\":3,\"dataFundacao\":\"1913-06-21T18:02:52.249Z\",\"nome\":\"Rio Branco Atlético Clube\"}";
		
		send(input1);
		send(input2);
		send(input3);
	}

	private static void send(String input) throws IOException {
		URL url = new URL("http://localhost:8080/timeCoracao/create");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		OutputStream os = connection.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		connection.disconnect();
	}

}
