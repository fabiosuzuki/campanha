package br.com;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.glassfish.jersey.servlet.ServletContainer;

import br.com.service.CampanhaService;
import br.com.service.SocioTorcedorService;
import br.com.service.TimeCoracaoService;

public class AppServer {

	private static final String JERSEY_CONFIG_SERVER_PROVIDER_CLASSNAMES = "jersey.config.server.provider.classnames";

	public static void main(String[] args) throws Exception {
		
		// Context //
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		
		// ThreadPool //
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(8);
        threadPool.setMaxThreads(200);
        threadPool.setIdleTimeout(60000);
        threadPool.setStopTimeout(5000);
        threadPool.setDetailedDump(false);
        
        // Server //
        Server server = new Server(threadPool);
        server.setHandler(context);

        // Connector //
        ServerConnector httpConnector = new ServerConnector(server);
        httpConnector.setHost("localhost");
        httpConnector.setPort(8080);
        httpConnector.setAcceptQueueSize(0);
        server.setConnectors(new Connector[]{httpConnector});
		

		ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);

		jerseyServlet.setInitParameter(JERSEY_CONFIG_SERVER_PROVIDER_CLASSNAMES, CampanhaService.class.getCanonicalName()+","+SocioTorcedorService.class.getCanonicalName()+","+TimeCoracaoService.class.getCanonicalName());

		try {
			server.start();
			server.join();
		} finally {
			server.destroy();
		}
	}

}
