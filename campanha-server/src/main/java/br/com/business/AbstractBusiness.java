package br.com.business;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.dao.CampanhaDaoImpl;
import br.com.dao.SocioTorcedorDaoImpl;
import br.com.dao.TimeCoracaoDaoImpl;

/**
 * Abstract Business
 *
 */
public abstract class AbstractBusiness {

	protected EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	protected SocioTorcedorDaoImpl socioTorcedorDao;
	protected CampanhaDaoImpl campanhaDao;
	protected TimeCoracaoDaoImpl timeCoracaoDao;

	public AbstractBusiness() {
		entityManagerFactory = Persistence.createEntityManagerFactory("testPersistenceUnit");
		entityManager = entityManagerFactory.createEntityManager();
		socioTorcedorDao = new SocioTorcedorDaoImpl(entityManager);
		campanhaDao = new CampanhaDaoImpl(entityManager);
		timeCoracaoDao = new TimeCoracaoDaoImpl(entityManager);
	}

	@PreDestroy
	private void closeConnections() {
		entityManagerFactory.close();
		entityManager.close();
	}

}
