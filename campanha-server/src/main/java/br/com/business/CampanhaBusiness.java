package br.com.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import br.com.entities.CampanhaEntity;
import br.com.utils.DateUtils;

/**
 * Campanha Business
 *
 */
public class CampanhaBusiness extends AbstractBusiness {

	public CampanhaEntity insertCampanha(CampanhaEntity campanhaEntity) {

		try {
			List<CampanhaEntity> list = campanhaDao.findCampanhasByStartAndEndDate(
					campanhaEntity.getDataInicioVigencia(), campanhaEntity.getDataFimVigencia());

			if (list.isEmpty()) {
				return persist(campanhaEntity);
			}

			boolean flag = false;
			for (CampanhaEntity entity : list) {
				if (campanhaEntity.getDataFimVigencia().equals(entity.getDataFimVigencia())) {
					flag = true;
					break;
				}
			}

			if (flag) {
				list.add(campanhaEntity);
				solveDates(campanhaEntity.getId(), list);
				for (CampanhaEntity campanha : list) {
					merge(campanha);
				}
				notificarSocios(list);
				return campanhaDao.getById(campanhaEntity.getId());
			} else {
				return persist(campanhaEntity);
			}

		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		return null;
	}

	public void solveDates(Long id, List<CampanhaEntity> campanhas) {

		if (id == null || id.equals(Long.valueOf(0))) {
			throw new IllegalArgumentException("Invalid ID.");
		}

		List<Date> dates = null;
		while (dates == null || !dates.isEmpty()) {
			for (CampanhaEntity campanha : campanhas) {
				if (!campanha.getId().equals(id)) {
					DateUtils.addOneDayDataFimVigencia(campanha);
				}
			}

			Map<Date, AtomicInteger> map = new HashMap<>();

			for (CampanhaEntity campanha : campanhas) {
				Date dataFimVigencia = campanha.getDataFimVigencia();

				AtomicInteger count = map.get(dataFimVigencia);
				if (count == null) {
					map.put(dataFimVigencia, new AtomicInteger(1));
				} else {
					map.get(dataFimVigencia).incrementAndGet();
				}
			}
			dates = map.entrySet().parallelStream().filter(i -> i.getValue().intValue() > 1).map(Map.Entry::getKey)
					.collect(Collectors.toList());
		}
	}

	private void merge(CampanhaEntity campanhaEntity) {
		entityManager.getTransaction().begin();
		campanhaDao.merge(campanhaEntity);
		entityManager.getTransaction().commit();
	}

	private CampanhaEntity persist(CampanhaEntity campanhaEntity) {
		entityManager.getTransaction().begin();
		campanhaDao.persist(campanhaEntity);
		entityManager.getTransaction().commit();
		return campanhaDao.getById(campanhaEntity.getId());
	}

	public CampanhaEntity updateCampanha(CampanhaEntity campanhaEntity) {
		try {
			entityManager.getTransaction().begin();
			campanhaDao.merge(campanhaEntity);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		return campanhaDao.getById(campanhaEntity.getId());
	}

	public CampanhaEntity removeCampanha(CampanhaEntity campanhaEntity) {
		try {
			entityManager.getTransaction().begin();
			campanhaDao.remove(campanhaEntity);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		return campanhaEntity;
	}

	public List<CampanhaEntity> findCampanhaById(CampanhaEntity campanhaEntity) {
		if (campanhaEntity.getId() == null || Long.valueOf(0).equals(campanhaEntity.getId())) {
			return campanhaDao.findAllCampanha();
		}
		List<CampanhaEntity> list = new ArrayList<>();
		CampanhaEntity entity = campanhaDao.getById(campanhaEntity.getId());
		if (entity != null) {
			list.add(entity);
		}
		return list;
	}

	public void notificarSocios(List<CampanhaEntity> campanhas) {
		for (CampanhaEntity campanha : campanhas) {
			System.out.println("Notificar Sócios que fazem parte da campanha " + campanha);
		}
	}

}
