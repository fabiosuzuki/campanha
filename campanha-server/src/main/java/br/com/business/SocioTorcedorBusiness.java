package br.com.business;

import java.util.List;

import br.com.entities.CampanhaEntity;
import br.com.entities.SocioTorcedorEntity;
import br.com.exception.SocioCadastradoException;

public class SocioTorcedorBusiness extends AbstractBusiness {

	public List<CampanhaEntity> cadastrar(SocioTorcedorEntity socioTorcedorEntity) {

		SocioTorcedorEntity socio = socioTorcedorDao.findByEmail(socioTorcedorEntity);
		if (socio != null) {
			throw new SocioCadastradoException("Cadastro já foi efetuado para este email.");
		}

		entityManager.getTransaction().begin();
		socioTorcedorDao.persist(socioTorcedorEntity);
		entityManager.getTransaction().commit();

		return campanhaDao.findAllCampanha();
	}

}
