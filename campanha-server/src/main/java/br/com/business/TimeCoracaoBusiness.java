package br.com.business;

import java.util.ArrayList;
import java.util.List;

import br.com.entities.TimeCoracaoEntity;

/**
 * Time Coracao Business
 *
 */
public class TimeCoracaoBusiness extends AbstractBusiness {

	public TimeCoracaoEntity persist(TimeCoracaoEntity timeCoracao) {
		timeCoracaoDao.persist(timeCoracao);
		return timeCoracaoDao.getById(timeCoracao.getId());
	}

	public TimeCoracaoEntity merge(TimeCoracaoEntity timeCoracao) {
		timeCoracaoDao.merge(timeCoracao);
		return timeCoracaoDao.getById(timeCoracao.getId());
	}

	public List<TimeCoracaoEntity> findTimeCoracaoById(TimeCoracaoEntity timeCoracao) {
		if (timeCoracao.getId() == null || Long.valueOf(0).equals(timeCoracao.getId())) {
			return timeCoracaoDao.findAll();
		}
		List<TimeCoracaoEntity> list = new ArrayList<>();
		TimeCoracaoEntity entity = timeCoracaoDao.getById(timeCoracao.getId());
		if (entity != null) {
			list.add(entity);
		}
		return list;
	}

	public TimeCoracaoEntity removeTimeCoracao(TimeCoracaoEntity timeCoracao) {
		try {
			entityManager.getTransaction().begin();
			timeCoracaoDao.remove(timeCoracao);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		return timeCoracao;
	}

}
