package br.com.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.entities.CampanhaEntity;

public class CampanhaDaoImpl extends GenericDAO<CampanhaEntity> {
	private static final long serialVersionUID = -2013580327550259437L;

	public CampanhaDaoImpl(EntityManager entityManager) {
		super(entityManager);
	}

	@SuppressWarnings("unchecked")
	public List<CampanhaEntity> findAllCampanha() {
		Query query = entityManager.createQuery("SELECT c FROM CampanhaEntity c WHERE TRUNC(c.dataFimVigencia) >= TRUNC(:data))");
		query.setParameter("data", Calendar.getInstance().getTime());
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CampanhaEntity> findCampanhasByStartAndEndDate(Date dataInicioVigencia, Date dataFimVigencia) {
		Query query = entityManager.createQuery("SELECT c FROM CampanhaEntity c WHERE TRUNC(c.dataInicioVigencia) >= TRUNC(:dataInicioVigencia) AND TRUNC(c.dataFimVigencia) <= TRUNC(:dataFimVigencia)");
		query.setParameter("dataInicioVigencia", dataInicioVigencia);
		query.setParameter("dataFimVigencia", dataFimVigencia);
		return query.getResultList();
	}

}
