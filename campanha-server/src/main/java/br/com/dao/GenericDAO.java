package br.com.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.entities.AbstractEntity;
import br.com.utils.ReflectionUtils;

@SuppressWarnings("unchecked")
public abstract class GenericDAO<T extends AbstractEntity> implements Serializable {
	private static final long serialVersionUID = -6942753122063550199L;

	protected EntityManager entityManager;

	public GenericDAO(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public T getById(final Long id) {
		return (T) entityManager.find(getTypeClass(), id);
	}

	public List<T> findAll() {
		return entityManager.createQuery("FROM " + getTypeClass().getName()).getResultList();
	}

	public void persist(final T entity) {
		entityManager.persist(entity);
	}

	public void merge(final T entity) {
		entityManager.merge(entity);
	}

	public void remove(T entity) {
		entity = (T) entityManager.find(getTypeClass(), entity.getId());
		entityManager.remove(entity);
	}

	public void removeById(final Long entityId) {
		T entity = this.getById(entityId);
		this.remove(entity);
	}

	public Class getTypeClass() {
		return ReflectionUtils.getTypeArguments(GenericDAO.class, getClass()).get(0);
	}

}
