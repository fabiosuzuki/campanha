package br.com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.entities.SocioTorcedorEntity;

public class SocioTorcedorDaoImpl extends GenericDAO<SocioTorcedorEntity> {
	private static final long serialVersionUID = 551878894034409302L;

	public SocioTorcedorDaoImpl(EntityManager entityManager) {
		super(entityManager);
	}

	public SocioTorcedorEntity findByEmail(SocioTorcedorEntity socioTorcedorEntity) {
		Query query = entityManager.createQuery("SELECT s FROM SocioTorcedorEntity s WHERE s.email = :email)");
		query.setParameter("email", socioTorcedorEntity.getEmail());

		List<SocioTorcedorEntity> list = query.getResultList();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
