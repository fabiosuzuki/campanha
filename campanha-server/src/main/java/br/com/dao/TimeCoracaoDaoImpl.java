package br.com.dao;

import javax.persistence.EntityManager;

import br.com.entities.TimeCoracaoEntity;

public class TimeCoracaoDaoImpl extends GenericDAO<TimeCoracaoEntity> {
	private static final long serialVersionUID = 5405950989122182973L;

	public TimeCoracaoDaoImpl(EntityManager entityManager) {
		super(entityManager);
	}

}
