package br.com.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "Campanha")
public class CampanhaEntity extends AbstractEntity {
	private static final long serialVersionUID = -7099170846207968393L;

	private String nomeCampanha;
	private Date dataInicioVigencia;
	private Date dataFimVigencia;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TIME_ID")
	private TimeCoracaoEntity timeCoracao;

	@ManyToMany(mappedBy = "campanhas", cascade = CascadeType.ALL)
	private List<SocioTorcedorEntity> socios = new ArrayList<SocioTorcedorEntity>();

	public String getNomeCampanha() {
		return nomeCampanha;
	}

	public void setNomeCampanha(String nomeCampanha) {
		this.nomeCampanha = nomeCampanha;
	}

	public Date getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(Date dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public Date getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(Date dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public TimeCoracaoEntity getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(TimeCoracaoEntity timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

}
