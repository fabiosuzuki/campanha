package br.com.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "SocioTorcedor")
public class SocioTorcedorEntity extends AbstractEntity {
	private static final long serialVersionUID = 3773223894336407861L;

	private String nomeCompleto;
	private String email;
	private Date dataNascimento;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TIME_ID")
	private SocioTorcedorEntity socioTorcedor;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SOCIO_CAMPANHA", joinColumns = @JoinColumn(name = "SOCIO_ID"), inverseJoinColumns = @JoinColumn(name = "CAMPANHA_ID"))
	private List<CampanhaEntity> campanhas = new ArrayList<CampanhaEntity>();

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public SocioTorcedorEntity getSocioTorcedor() {
		return socioTorcedor;
	}

	public void setSocioTorcedor(SocioTorcedorEntity socioTorcedor) {
		this.socioTorcedor = socioTorcedor;
	}

}
