package br.com.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "timecoracao")
public class TimeCoracaoEntity extends AbstractEntity {
	private static final long serialVersionUID = -1115966568029833993L;

	private String nome;
	private Date dataFundacao;

	@OneToMany(mappedBy = "timeCoracao", cascade = CascadeType.ALL)
	private List<CampanhaEntity> campanhas;
	
	@OneToMany(mappedBy = "socioTorcedor", cascade = CascadeType.ALL)
	private List<SocioTorcedorEntity> socios;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataFundacao() {
		return dataFundacao;
	}

	public void setDataFundacao(Date dataFundacao) {
		this.dataFundacao = dataFundacao;
	}

	public List<CampanhaEntity> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(List<CampanhaEntity> campanhas) {
		this.campanhas = campanhas;
	}

}
