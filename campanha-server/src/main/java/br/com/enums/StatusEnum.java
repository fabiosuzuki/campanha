package br.com.enums;

public enum StatusEnum {

	ATIVO  ("A", "Ativo"), 
	INATIVO("I", "Inativo");

	private StatusEnum(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	private String codigo;
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public String getCodigo() {
		return codigo;
	}

}
