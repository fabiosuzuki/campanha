package br.com.exception;

public class SocioCadastradoException extends RuntimeException {
	private static final long serialVersionUID = 3993755102518891116L;

	public SocioCadastradoException(String message) {
		super(message);
	}

}
