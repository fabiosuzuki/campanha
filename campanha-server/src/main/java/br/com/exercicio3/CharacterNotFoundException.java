package br.com.exercicio3;

/**
 * Exception used when a Character is Not Found
 *
 */
public class CharacterNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -8802367459592383269L;

	public CharacterNotFoundException(String message) {
		super(message);
	}

}
