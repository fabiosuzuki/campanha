package br.com.exercicio3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Exercicio3 {

	private static final String vowels = "aeiouAEIOU";

	/**
	 * Return a Character according to specification. If don't find throws a CharacterNotFoundException 
	 * 
	 * @param input
	 * @return
	 */
	public static char firstChar(Stream input) {

		Map<Character, AtomicInteger> map = new HashMap<>();

		// Populate a map Key with vowels (consonants will be ignored) and the Value with the occurrences numbers //
		while (input.hasNext()) {
			char c = input.getNext();
			if (vowels.contains(String.valueOf(c))) {
				AtomicInteger count = map.get(c);
				if (count == null) {
					map.put(c, new AtomicInteger(1));
				} else {
					map.get(c).incrementAndGet();
				}
			}
		}

		// Filters Characters with only one occurrence //
		List<Character> characters = map.entrySet().parallelStream().filter(i -> i.getValue().intValue() == 1).map(Map.Entry::getKey).collect(Collectors.toList());
		String word = input.getWord();

		// Search for Character according to specification //
		for (Character character : characters) {
			int index = word.indexOf(String.valueOf(character));
			char a = 0;
			char b = 0;
			char c = word.charAt(index);

			try {
				b = word.charAt(index - 1);
				a = word.charAt(index - 2);
			} catch (StringIndexOutOfBoundsException e) {
				continue;
			}

			// * * a b c * *
			//     ^ ^ ^
			//     | | | 
			//     | | -- first vowel without repetition
			//     | |--- a consonant
			//     |----- a vowel
			if (!vowels.contains(String.valueOf(b)) && vowels.contains(String.valueOf(a))) {
				return c;
			}
		}

		// If don't find throws a CharacterNotFoundException //
		throw new CharacterNotFoundException("Character not found in text.");
	}

}
