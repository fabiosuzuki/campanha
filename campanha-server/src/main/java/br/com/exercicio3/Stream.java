package br.com.exercicio3;

/**
 * Stream Interface
 *
 */
public interface Stream {
	public char getNext();
	public boolean hasNext();
	public String getWord();
}
