package br.com.exercicio3;

/**
 * Implementation of Stream interface
 *
 */
public class StreamImpl implements Stream {

	private final String word;
	private final char[] array;
	private final int length;
	private int index = 0;

	StreamImpl(String word) {

		if (word == null || "".equals(word.trim())) {
			throw new IllegalArgumentException("The String input must not be empty");
		}

		this.word = word;
		this.array = word.toCharArray();
		this.length = word.length();
	}

	@Override
	public char getNext() {
		return array[index++];
	}

	@Override
	public boolean hasNext() {
		return index < length;
	}

	@Override
	public String getWord() {
		return word;
	}

}