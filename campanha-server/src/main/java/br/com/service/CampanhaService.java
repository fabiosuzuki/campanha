package br.com.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.business.CampanhaBusiness;
import br.com.entities.CampanhaEntity;

@Path("/campanha")
public class CampanhaService {

	private CampanhaBusiness cb = new CampanhaBusiness();

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CampanhaEntity insert(CampanhaEntity campanhaEntity) {
		return cb.insertCampanha(campanhaEntity);
	}

	@POST
	@Path("/read")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<CampanhaEntity> read(CampanhaEntity campanhaEntity) {
		return cb.findCampanhaById(campanhaEntity);
	}

	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CampanhaEntity update(CampanhaEntity campanhaEntity) {
		return cb.updateCampanha(campanhaEntity);
	}

	@POST
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CampanhaEntity delete(CampanhaEntity campanhaEntity) {
		return cb.removeCampanha(campanhaEntity);
	}

}
