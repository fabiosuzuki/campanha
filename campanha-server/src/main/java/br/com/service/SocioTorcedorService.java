package br.com.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.business.SocioTorcedorBusiness;
import br.com.entities.CampanhaEntity;
import br.com.entities.SocioTorcedorEntity;

@Path("/socioTorcedor")
public class SocioTorcedorService {

	private SocioTorcedorBusiness stb = new SocioTorcedorBusiness();

	@POST
	@Path("/cadastrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<CampanhaEntity> cadastrar(SocioTorcedorEntity socioTorcedorEntity) {
		return stb.cadastrar(socioTorcedorEntity);
	}

}
