package br.com.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.business.TimeCoracaoBusiness;
import br.com.entities.TimeCoracaoEntity;

@Path("/timeCoracao")
public class TimeCoracaoService {

	private TimeCoracaoBusiness tb = new TimeCoracaoBusiness();

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TimeCoracaoEntity insert(TimeCoracaoEntity timeCoracao) {
		return tb.persist(timeCoracao);
	}

	@POST
	@Path("/read")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<TimeCoracaoEntity> read(TimeCoracaoEntity timeCoracao) {
		return tb.findTimeCoracaoById(timeCoracao);
	}

	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TimeCoracaoEntity update(TimeCoracaoEntity timeCoracao) {
		return tb.merge(timeCoracao);
	}

	@POST
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TimeCoracaoEntity delete(TimeCoracaoEntity timeCoracao) {
		return tb.removeTimeCoracao(timeCoracao);
	}

}
