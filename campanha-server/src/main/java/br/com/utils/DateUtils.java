package br.com.utils;

import java.util.Calendar;

import br.com.entities.CampanhaEntity;

public class DateUtils {

	private DateUtils() {
	}

	public static void addOneDayDataFimVigencia(CampanhaEntity campanha) {
		
		if (campanha == null || campanha.getDataFimVigencia() == null) {
			throw new IllegalArgumentException("Objeto CampanhaEntity ou campo DataFimVigencia nulo.");
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(campanha.getDataFimVigencia());
		calendar.add(Calendar.DATE, 1);
		campanha.setDataFimVigencia(calendar.getTime());
	}
	
}
