package br.com;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.entities.CampanhaEntity;
import br.com.enums.StatusEnum;

public abstract class AbstractTest {

	protected CampanhaEntity createCampanhaEntity(Long id, String nomeCampanha, StatusEnum statusEnum, String dataInicioVigencia, String dataFimVigencia) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		CampanhaEntity campanha = new CampanhaEntity();
		campanha.setId(id);
		campanha.setNomeCampanha(nomeCampanha);
		campanha.setStatus(statusEnum.getDescricao());
		campanha.setDataInicioVigencia(sdf.parse(dataInicioVigencia));
		campanha.setDataFimVigencia(sdf.parse(dataFimVigencia));
		return campanha;
	}

}
