package br.com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import br.com.business.CampanhaBusiness;
import br.com.entities.CampanhaEntity;
import br.com.enums.StatusEnum;
import junit.framework.Assert;

public class CampanhaBusinessTest extends AbstractTest {

	@Test
	public void addOneDayDataFimVigenciaTest1() throws ParseException {

		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		CampanhaEntity campanha1 = createCampanhaEntity(1L, "Campanha 1", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");
		CampanhaEntity campanha2 = createCampanhaEntity(2L, "Campanha 2", StatusEnum.ATIVO, "2017-10-01", "2017-10-04");
		CampanhaEntity campanha3 = createCampanhaEntity(3L, "Campanha 3", StatusEnum.ATIVO, "2017-10-01", "2017-10-05");
		CampanhaEntity campanha4 = createCampanhaEntity(4L, "Campanha 4", StatusEnum.ATIVO, "2017-10-01", "2017-10-06");
		CampanhaEntity campanha5 = createCampanhaEntity(5L, "Campanha 5", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");

		CampanhaBusiness cb = new CampanhaBusiness();
		cb.solveDates(5L, Arrays.asList(campanha1, campanha2, campanha3, campanha4, campanha5));

		Assert.assertEquals(sdf.parse("2017-10-04"), campanha1.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-05"), campanha2.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-06"), campanha3.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-07"), campanha4.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-03"), campanha5.getDataFimVigencia());
	}
	
	@Test
	public void addOneDayDataFimVigenciaTest2() throws ParseException {

		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		CampanhaEntity campanha1 = createCampanhaEntity(1L, "Campanha 1", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");
		CampanhaEntity campanha2 = createCampanhaEntity(2L, "Campanha 2", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");

		CampanhaBusiness cb = new CampanhaBusiness();
		cb.solveDates(2L, Arrays.asList(campanha1, campanha2));

		Assert.assertEquals(sdf.parse("2017-10-04"), campanha1.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-03"), campanha2.getDataFimVigencia());
	}

	@Test(expected = IllegalArgumentException.class)
	public void addOneDayDataFimVigenciaNullTest1() throws ParseException {
		new CampanhaBusiness().solveDates(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addOneDayDataFimVigenciaNullTest2() throws ParseException {
		new CampanhaBusiness().solveDates(null, new ArrayList<>());
	}

	@Test
	public void cenarioDaProvaTest() throws ParseException {

		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		CampanhaEntity campanha1 = createCampanhaEntity(1L, "Campanha 1", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");
		CampanhaEntity campanha2 = createCampanhaEntity(2L, "Campanha 2", StatusEnum.ATIVO, "2017-10-01", "2017-10-02");
		CampanhaEntity campanha3 = createCampanhaEntity(3L, "Campanha 3", StatusEnum.ATIVO, "2017-10-01", "2017-10-03");

		CampanhaBusiness cb = new CampanhaBusiness();
		cb.solveDates(3L, Arrays.asList(campanha1, campanha2, campanha3));

		Assert.assertEquals(sdf.parse("2017-10-05"), campanha1.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-04"), campanha2.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-03"), campanha3.getDataFimVigencia());
		Assert.assertEquals(sdf.parse("2017-10-01"), campanha1.getDataInicioVigencia());
		Assert.assertEquals(sdf.parse("2017-10-01"), campanha2.getDataInicioVigencia());
		Assert.assertEquals(sdf.parse("2017-10-01"), campanha3.getDataInicioVigencia());
	}

}
