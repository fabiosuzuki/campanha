package br.com;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import br.com.entities.CampanhaEntity;
import br.com.enums.StatusEnum;
import br.com.utils.DateUtils;
import junit.framework.Assert;

public class DateUtilsTest extends AbstractTest {

	@Test
	public void addOneDayDataFimVigenciaTest() throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		CampanhaEntity campanha = createCampanhaEntity(1L, "Campanha 1", StatusEnum.ATIVO, "2017-10-01", "2017-10-02");

		Assert.assertEquals(sdf.parse("2017-10-02"), campanha.getDataFimVigencia());

		DateUtils.addOneDayDataFimVigencia(campanha);
		Assert.assertEquals(sdf.parse("2017-10-03"), campanha.getDataFimVigencia());

		DateUtils.addOneDayDataFimVigencia(campanha);
		DateUtils.addOneDayDataFimVigencia(campanha);
		Assert.assertEquals(sdf.parse("2017-10-05"), campanha.getDataFimVigencia());

		DateUtils.addOneDayDataFimVigencia(campanha);
		DateUtils.addOneDayDataFimVigencia(campanha);
		DateUtils.addOneDayDataFimVigencia(campanha);
		Assert.assertEquals(sdf.parse("2017-10-08"), campanha.getDataFimVigencia());

	}

	@Test(expected = IllegalArgumentException.class)
	public void addOneDayDataFimVigenciaCampanhaNullTest() throws ParseException {
		DateUtils.addOneDayDataFimVigencia(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addOneDayDataFimVigenciaDataFimVigenciaNullTest() throws ParseException {

		CampanhaEntity campanha = createCampanhaEntity(1L, "Campanha 1", StatusEnum.ATIVO, "2017-10-01", "2017-10-02");
		campanha.setDataInicioVigencia(null);
		campanha.setDataFimVigencia(null);
		DateUtils.addOneDayDataFimVigencia(campanha);
	}

}
