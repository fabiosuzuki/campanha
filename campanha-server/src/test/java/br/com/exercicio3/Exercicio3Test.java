package br.com.exercicio3;

import org.junit.Test;

import junit.framework.Assert;

public class Exercicio3Test {

	@Test
	public void firstCharTest1() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aAbBABacafe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test
	public void firstCharTest2() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aAbBABiacaf"));
		Assert.assertEquals('i', firstChar);
	}
	
	@Test
	public void firstCharTest3() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aAbBA   Bacafe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test
	public void firstCharTest4() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("  aAbBA   Bac afe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = CharacterNotFoundException.class)
	public void firstCharTest5() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("  aAbBA   Bac a fe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test
	public void firstCharTest6() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aAbBABacafeafi"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = CharacterNotFoundException.class)
	public void firstCharCharacterNotFoundTest() {
		Exercicio3.firstChar(new StreamImpl("aAbBABacaf"));
	}

	@Test
	public void firstCharWithNumberTest() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aAbBABacafe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test
	public void firstCharWithSpecialCharacterTest1() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aA???*bBA  B%%% an acafe"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = CharacterNotFoundException.class)
	public void firstCharWithSpecialCharacterTest2() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("aA???*bBA  B% an acaf"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = CharacterNotFoundException.class)
	public void firstCharWithSpecialCharacterTest3() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("#"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = CharacterNotFoundException.class)
	public void firstCharWithSpecialCharacterTest4() {
		char firstChar = Exercicio3.firstChar(new StreamImpl("#?@$¨&*()_+"));
		Assert.assertEquals('e', firstChar);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void firstCharEmptyTest1() {
		Exercicio3.firstChar(new StreamImpl(""));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void firstCharEmptyTest2() {
		Exercicio3.firstChar(new StreamImpl("     "));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void firstCharNullTest() {
		Exercicio3.firstChar(new StreamImpl(null));
	}

}
